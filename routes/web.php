<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products', 'ProductController');

Route::get('product_stock/create/{id}', 'ProductStockController@create')->name('product_stock.create');
Route::post('product_stock/store', 'ProductStockController@store')->name('product_stock.store');

Route::get('order/create/{id}', 'OrderController@create')->name('order.create');
Route::post('order/store', 'OrderController@store')->name('order.store');
