<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function create(Request $request)
    {
        $product = Product::find($request->id);

        return view('orders.create', compact('product'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'qty' => 'required'
        ]);

        //update stock
        $product = Product::where('id', $request->product_id)
            ->update(['stock' => $request->existing_stock - $request->qty]);

        Order::create([
            'product_id' => $request->product_id,
            'qty'        => $request->qty
        ]);

        return redirect()->route('products.index')->with('success', 'successfully');
    }
}
