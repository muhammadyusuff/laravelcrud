<?php

namespace App\Http\Controllers;

use App\ProductStock;
use App\Product;
use Illuminate\Http\Request;

class ProductStockController extends Controller
{
    public function create(Request $request)
    {
        $product = Product::find($request->id);

        return view('product-stocks.create', compact('product'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'stock' => 'required'
        ]);

        //update stock
        $product = Product::where('id', $request->product_id)
            ->update(['stock' => $request->existing_stock + $request->stock]);

        ProductStock::create($request->all());

        return redirect()->route('products.index')->with('success', 'Product Stock added successfully');
    }
}
